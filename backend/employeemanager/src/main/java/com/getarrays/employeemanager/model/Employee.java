package com.getarrays.employeemanager.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String email;
    private String jobTitle;
    private String phone;
    private String imageUrl;
    @Column(nullable = false, updatable = false)
    private String employeeCode;

    public static List<Employee> toList(List<Employee> findAll) {

        return findAll.stream().map(m -> {

            return Employee.builder().id(m.id).email(m.email).employeeCode(m.employeeCode).imageUrl(m.imageUrl)
                    .jobTitle(m.jobTitle).name(m.name).phone(m.phone).build();
        }).collect(Collectors.toList());

    }

    public void update(Employee request){
        this.name=request.getName();
        this.email=request.getEmail();
        this.employeeCode=request.getEmployeeCode();
        this.phone=request.getPhone();
        this.jobTitle=request.getJobTitle();
        this.imageUrl=request.getImageUrl();
    }

    public Employee toDto() {
        return Employee.builder().id(this.id).email(this.email).employeeCode(this.employeeCode).imageUrl(this.imageUrl)
                .jobTitle(this.jobTitle).name(this.name).phone(this.phone).build();
    }
}
