package com.getarrays.employeemanager.repository;

import com.getarrays.employeemanager.model.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface EmployeeRepository {

    @Insert("INSERT INTO employee " + "(email,employeeCode,imageUrl,jobTitle,name,phone) "
            + "VALUES " + "(#{email}," + "#{employeeCode}," + "#{imageUrl}," + "#{jobTitle}," + "#{name},"
            + "#{phone})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    Integer save(Employee employee);

    @Select("SELECT * FROM employee WHERE name=#{name}")
    Optional<Employee> findByName(String name);

    @Select("SELECT * FROM employee WHERE id=#{id}")
    Optional<Employee> findById(Long id);

    @Select("SELECT * FROM employee LIMIT #{limit} offset #{page}")
    List<Employee> findAll(int page, int limit);

    @Delete("DELETE FROM employee WHERE id=#{id}")
    void deleteById(Long id);

    @Update("UPDATE employee SET email=#{email},employeeCode=#{employeeCode}," +
            "imageUrl=#{imageUrl},jobTitle=#{jobTitle}," +
            "name=#{name},phone=#{phone}"+
            " WHERE id=#{id}")
    void update(Employee employee);
}