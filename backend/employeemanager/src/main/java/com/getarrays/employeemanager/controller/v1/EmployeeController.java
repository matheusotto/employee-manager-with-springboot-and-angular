package com.getarrays.employeemanager.controller.v1;

import com.getarrays.employeemanager.model.Employee;
import com.getarrays.employeemanager.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/employee")
public class EmployeeController {

    EmployeeService service;

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<Employee> findAll(
            @RequestParam(required = false, value = "page", defaultValue = "0") int page,
            @RequestParam(required = false, value = "limit", defaultValue = "10") int limit) {
        return service.findAll(page, limit);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee getEmployeeById(@PathVariable("id") Long id) {
        return service.findById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable("id") Long id) {
        this.service.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Employee updateEmployee(@RequestBody Employee request, @PathVariable("id") Long id) {
        return service.update(request, id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createPerson(@RequestBody Employee request) {
        return service.addEmployee(request);
    }
}
