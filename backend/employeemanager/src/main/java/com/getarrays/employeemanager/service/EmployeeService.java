package com.getarrays.employeemanager.service;

import com.getarrays.employeemanager.exception.UserNotFoundException;
import com.getarrays.employeemanager.model.Employee;
import com.getarrays.employeemanager.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class EmployeeService {
    private final EmployeeRepository repository;

    public Employee addEmployee(Employee employee) {

        repository.save(employee);

        return employee;

    }

    public List<Employee> findAll(int page, int limit) {

        List<Employee> findAll = repository.findAll(page, limit);

        log.info("method=findAll page={} limit={}", page, limit);

        return Employee.toList(findAll);
    }

    public Employee update( Employee request, Long id) {

        Employee employee = repository.findById(id).orElseThrow(() ->
                new UserNotFoundException("User byd id " + id + " was not found"));

        employee.update(request);

        request.update(employee);

        log.info("method=update personId={}", employee.getId());

        return employee.toDto();

    }

    public void deleteById(Long id) {

        repository.deleteById(id);

        log.info("method=deleteById personId={}", id);

    }

    public Employee findById(Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User byd id " + id + " was not found"));

    }
}